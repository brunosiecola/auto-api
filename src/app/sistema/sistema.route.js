
const express = require('express');
const router = express.Router();

const loginRouter = require('./login/login.route');
const lojaRouter = require('./loja/loja.route');
const usuarioRouter = require('./usuario/usuario.route');
const usuariosRouter = require('./usuarios/usuarios.route');
const funcionariosRouter = require('./funcionarios/funcionarios.route');
const termosRouter = require('./termos/termos.route');
const clientesRouter = require('./clientes/clientes.route');
const marcasRouter = require('./marcas/marcas.route');
const combustiveisRouter = require('./combustiveis/combustiveis.route');
const coresRouter = require('./cores/cores.route');
const veiculosRouter = require('./veiculos/veiculos.route');

router.use('/', loginRouter);
router.use('/', lojaRouter);
router.use('/', usuarioRouter);
router.use('/', usuariosRouter);
router.use('/', funcionariosRouter);
router.use('/', termosRouter);
router.use('/', clientesRouter);
router.use('/', marcasRouter);
router.use('/', combustiveisRouter);
router.use('/', coresRouter);
router.use('/', veiculosRouter);

module.exports = router;
