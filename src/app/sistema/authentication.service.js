
const jsonwebtoken = require('jsonwebtoken');
const config = require('./../../config');
const Response = require('./../../models/response.model');

module.exports = {

  authenticate: (req, res, next) => {

    const token = req.headers['token'];

    if(token) {
      jsonwebtoken.verify(token, config.jsonwebtoken.secret, (error, decoded) => {
        if(!error) {
          res.token = decoded;
          next();
        }
        else {
          res.status(401).send(new Response(false, 'A autenticação do token falhou.', null));
        }
      });
    }
    else {
      res.status(400).send(new Response(false, 'O token precisa ser informado.', null));
    }

  }

};