
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');

module.exports = {

  get: (req, res) => {
    let filter, search, entries, query;
    filter = req.query.filter || '';
    search = req.query.search || '';
    if(filter && filter === 'nome') {
      if(filter === 'nome') search = '%' + search + '%';
      entries = [filter, search];
      query = 'SELECT id, nome FROM marca WHERE ?? LIKE ?';
    }
    else {
      entries = [];
      query = 'SELECT id, nome FROM marca';
    }
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query(query, entries, (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'As marcas foram retornadas com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id, nome FROM marca WHERE id = ?', [req.params.codigo], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'A marca foi retornada com sucesso.', results[0]));
      });
    });
  }

};