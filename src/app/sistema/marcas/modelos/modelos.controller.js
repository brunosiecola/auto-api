
const pool = require('./../../../../database');
const Response = require('./../../../../models/response.model');

module.exports = {

  get: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id, nome FROM modelo WHERE marca_id = ?', [req.params.marca_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os modelos da marca foram retornados com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id, nome FROM modelo WHERE marca_id = ? AND id = ?', [req.params.marca_id, req.params.modelo_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O modelo foi retornado com sucesso.', results));
      });
    });
  }

};