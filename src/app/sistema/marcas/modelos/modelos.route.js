
const express = require('express');
const router = express.Router({ mergeParams: true });
const service = require('./../../authentication.service');
const controller = require('./modelos.controller');

router.get('/modelos', service.authenticate, controller.get);
router.get('/modelos/:modelo_id', service.authenticate, controller.getByCodigo);

module.exports = router;