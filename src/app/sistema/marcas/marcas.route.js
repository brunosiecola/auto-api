
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./marcas.controller');
const modelosRouter = require('./modelos/modelos.route');

router.get('/marcas', service.authenticate, controller.get);
router.get('/marcas/:marca_id', service.authenticate, controller.getByCodigo);

router.use('/marcas/:marca_id', modelosRouter);

module.exports = router;