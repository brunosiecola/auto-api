
const DataValidator = require('./../../../validators/data.validator');

class LoginModel extends DataValidator {

  constructor(login) {
    super();
    this.login = {
      cnpj: login.cnpj || null,
      usuario: login.usuario || null,
      senha: login.senha || null
    }
  }

  validateCnpj() {
    this.isRequired(this.login.cnpj, 'O campo cnpj deve ser informado.');
    if(this.login.cnpj) {
      this.isFixedLength(this.login.cnpj, 15, 'O campo cnpj deve ter 15 caracteres.');
    }
  }

  validateUsuario() {
    this.isRequired(this.login.usuario, 'O campo usuario deve ser informado.');
    if(this.login.usuario) {
      this.hasMaxLength(this.login.usuario, 20, 'O campo usuario pode ter até 100 caracteres.');
    }
  }

  validateSenha() {
    this.isRequired(this.login.senha, 'O campo senha deve ser informado.');
    if(this.login.senha) {
      this.hasMaxLength(this.login.senha, 20, 'O campo senha pode ter até 20 caracteres.');
    }
  }

  validateToLogin() {
    this.validateCnpj();
    this.validateUsuario();
    this.validateSenha();
  }

  transformToLogin() {
    this.login.usuario = this.setUpperCase(this.login.usuario);
  }

}

module.exports = LoginModel;
