
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const config = require('./../../../config');
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const LoginModel = require('./login.model');

module.exports = {

  post: (req, res) => {

    const loginModel = new LoginModel(req.body);
    loginModel.validateToLogin();

    if(!loginModel.isValid())
      return res.send(new Response(false, loginModel.errors, null));

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT usuario.id as usuario_id, usuario.senha as usuario_senha, loja.id as loja_id FROM usuario INNER JOIN loja ON usuario.loja_id = loja.id WHERE loja.cnpj = ? AND usuario = ?', [loginModel.login.cnpj, loginModel.login.usuario], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        if(!results[0]) return res.send(new Response(false, 'Os campos CNPJ, usuário ou senha podem estar incorretos, verifique e tente novamente.', null));
        if(!bcrypt.compareSync(loginModel.login.senha, results[0].usuario_senha)) return res.send(new Response(false, 'Os campos CNPJ, usuário ou senha podem estar incorretos, verifique e tente novamente.', null));
        const token = jsonwebtoken.sign({
          loja_id: results[0].loja_id,
          usuario_id: results[0].usuario_id
        }, config.jsonwebtoken.secret, { expiresIn: 86400 });
        res.send(new Response(true, 'Sua autenticação foi realizada com sucesso.', { token: token }));
      });
    });
    

  }

};
