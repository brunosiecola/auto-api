
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./cores.controller');

router.get('/cores', service.authenticate, controller.get);
router.get('/cores/:cor_id', service.authenticate, controller.getByCodigo);

module.exports = router;