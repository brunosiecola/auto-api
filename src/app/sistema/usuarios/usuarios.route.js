
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./usuarios.controller');
const permissoesRouter = require('./permissoes/permissoes.route');

router.get('/usuarios', service.authenticate, controller.get);
router.get('/usuarios/:codigo', service.authenticate, controller.getByCodigo);
router.post('/usuarios', service.authenticate, controller.post);
router.put('/usuarios/:codigo', service.authenticate, controller.put);
router.delete('/usuarios/:codigo', service.authenticate, controller.delete);

router.use('/usuarios/:codigo', permissoesRouter);

module.exports = router;
