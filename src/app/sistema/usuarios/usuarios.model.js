
const DataValidator = require('./../../../validators/data.validator');

class UsuariosModel extends DataValidator {

  constructor(usuario) {
    super();
    this.usuario = {
      nome: usuario.nome || null,
      usuario: usuario.usuario || null,
      senha: usuario.senha || null,
      status: usuario.status || null
    }
  }

  validateNome() {
    this.isRequired(this.usuario.nome, 'O campo nome deve ser informado.');
    if(this.usuario.nome) {
      this.hasMaxLength(this.usuario.nome, 100, 'O campo nome pode ter até 100 caracteres.');
    }
  }

  validateUsuario() {
    this.isRequired(this.usuario.usuario, 'O campo usuario deve ser informado.');
    if(this.usuario.usuario) {
      this.hasMaxLength(this.usuario.usuario, 20, 'O campo usuario pode ter até 100 caracteres.');
    }
  }

  validateSenha() {
    this.isRequired(this.usuario.senha, 'O campo senha deve ser informado.');
    if(this.usuario.senha) {
      this.hasMaxLength(this.usuario.senha, 20, 'O campo senha pode ter até 20 caracteres.');
    }
  }

  validateStatus() {
    this.isRequired(this.usuario.status, 'O campo status deve ser informado.');
    if(this.usuario.status && (this.usuario.status != '0' && this.usuario.status != '1')) {
      this.errors.push('O campo status deve ter como valor 0 ou 1.');
    }
  }

  validateToInsert() {
    this.validateNome();
    this.validateUsuario();
    this.validateSenha();
    this.validateStatus();
  }

  validateToUpdate() {
    this.validateNome();
    this.validateUsuario();
    if(this.usuario.senha) {
      this.validateSenha();
    }
  }

  transformToInsert() {
    this.usuario.nome = this.setUpperCase(this.usuario.nome);
    this.usuario.usuario = this.setUpperCase(this.usuario.usuario);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = UsuariosModel;
