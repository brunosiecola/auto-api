
const bcrypt = require('bcrypt');
const config = require('./../../../config');
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const UsuariosModel = require('./usuarios.model');

module.exports = {

  post: (req, res) => {
    const usuariosModel = new UsuariosModel(req.body);
    usuariosModel.validateToInsert();

    if(!usuariosModel.isValid())
      return res.send(new Response(false, usuariosModel.errors, null));
    
    usuariosModel.transformToInsert();
    usuariosModel.usuario.senha = bcrypt.hashSync(usuariosModel.usuario.senha, config.bcrypt.salt);
    usuariosModel.usuario.loja_id = res.token.loja_id;
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('INSERT INTO usuario SET ?', [usuariosModel.usuario], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('SELECT codigo FROM usuario WHERE id = ?', [results.insertId], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O usuário foi adicionado com sucesso.', results[0]));
        });
      });
    });
    
  },

  put: (req, res) => {
    const usuariosModel = new UsuariosModel(req.body);
    usuariosModel.validateToUpdate();

    if(!usuariosModel.isValid())
      return res.send(new Response(false, usuariosModel.errors, null));

    if(usuariosModel.usuario.senha)
      usuariosModel.usuario.senha = bcrypt.hashSync(usuariosModel.usuario.senha, config.bcrypt.salt);
    else
      delete usuariosModel.usuario.senha;

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      usuariosModel.transformToUpdate();
      connection.query('UPDATE usuario SET ? WHERE codigo = ? AND loja_id = ?', [usuariosModel.usuario, req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O usuário foi alterado com sucesso.', results));
      });
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('DELETE FROM usuario WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O usuário foi excluído com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    let filter, search, entries, query;
    filter = req.query.filter || '';
    search = req.query.search || '';
    if(filter && (filter === 'codigo' || filter === 'nome' || filter === 'usuario' || filter === 'status')) {
      if(filter === 'nome' || filter === 'usuario') search = '%' + search + '%';
      entries = [res.token.loja_id, filter, search];
      query = 'SELECT codigo, nome, usuario, status FROM usuario WHERE loja_id = ? AND ?? LIKE ?';
    }
    else {
      entries = [res.token.loja_id];
      query = 'SELECT codigo, nome, usuario, status FROM usuario WHERE loja_id = ?';
    }
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query(query, entries, (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os usuários foram retornados com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT codigo, nome, usuario, status FROM usuario WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O usuário foi retornado com sucesso.', results[0]));
      });
    });
  }

};
