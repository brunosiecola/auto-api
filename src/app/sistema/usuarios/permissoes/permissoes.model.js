
const DataValidator = require('./../../../../validators/data.validator');

class UsuarioPermissaoModel extends DataValidator {

  constructor(usuario_permissao) {
    super();
    this.usuario_permissao = {
      cliente_adicionar: usuario_permissao.cliente_adicionar,
      cliente_alterar: usuario_permissao.cliente_alterar,
      cliente_excluir: usuario_permissao.cliente_excluir,
      cliente_visualizar: usuario_permissao.cliente_visualizar
    }
  }

  validateClienteAdicionar() {
    this.isRequired(this.usuario_permissao.cliente_adicionar, 'O campo cliente_adicionar deve ser informado.');
    if(this.usuario_permissao.cliente_adicionar && (this.usuario_permissao.cliente_adicionar != '0' && this.usuario_permissao.cliente_adicionar != '1')) {
      this.errors.push('O campo cliente_adicionar deve ter como valor 0 ou 1.');
    }
  }

  validateClienteAlterar() {
    this.isRequired(this.usuario_permissao.cliente_alterar, 'O campo cliente_alterar deve ser informado.');
    if(this.usuario_permissao.cliente_alterar && (this.usuario_permissao.cliente_alterar != '0' && this.usuario_permissao.cliente_alterar != '1')) {
      this.errors.push('O campo cliente_alterar deve ter como valor 0 ou 1.');
    }
  }

  validateClienteExcluir() {
    this.isRequired(this.usuario_permissao.cliente_excluir, 'O campo cliente_excluir deve ser informado.');
    if(this.usuario_permissao.cliente_excluir && (this.usuario_permissao.cliente_excluir != '0' && this.usuario_permissao.cliente_excluir != '1')) {
      this.errors.push('O campo cliente_excluir deve ter como valor 0 ou 1.');
    }
  }

  validateClienteVisualizar() {
    this.isRequired(this.usuario_permissao.cliente_visualizar, 'O campo cliente_visualizar deve ser informado.');
    if(this.usuario_permissao.cliente_visualizar && (this.usuario_permissao.cliente_visualizar != '0' && this.usuario_permissao.cliente_visualizar != '1')) {
      this.errors.push('O campo cliente_visualizar deve ter como valor 0 ou 1.');
    }
  }

  validateToUpdate() {
    this.validateClienteAdicionar();
    this.validateClienteAlterar();
    this.validateClienteExcluir();
    this.validateClienteVisualizar();
  }

}

module.exports = UsuarioPermissaoModel;
