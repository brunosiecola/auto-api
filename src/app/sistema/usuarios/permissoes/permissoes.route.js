
const express = require('express');
const router = express.Router({ mergeParams: true });
const service = require('./../../authentication.service');
const controller = require('./permissoes.controller');

router.get('/permissoes', service.authenticate, controller.get);
router.put('/permissoes', service.authenticate, controller.put);

module.exports = router;
