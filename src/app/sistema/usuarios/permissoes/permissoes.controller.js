
const pool = require('./../../../../database');
const Response = require('./../../../../models/response.model');
const PermissoesModel = require('./permissoes.model');

module.exports = {

  get: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM usuario WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('SELECT cliente_adicionar, cliente_alterar, cliente_excluir, cliente_visualizar FROM usuario_permissao WHERE usuario_id = ?', [results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'As permissões do usuário foram retornadas com sucesso.', results[0]));
        });
      });
    });
  },

  put: (req, res) => {
    const permissoesModel = new PermissoesModel(req.body);
    permissoesModel.validateToUpdate();

    if(!permissoesModel.isValid())
      return res.send(new Response(false, permissoesModel.errors, null));
    
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM usuario WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('UPDATE usuario_permissao SET ? WHERE usuario_id = ?', [permissoesModel.usuario_permissao, results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'As permissões do usuário foram alteradas com sucesso.', results));
        });
      });
    });
  }

};
