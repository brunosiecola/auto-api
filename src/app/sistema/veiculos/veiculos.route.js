
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./veiculos.controller');

router.get('/veiculos', service.authenticate, controller.get);
router.get('/veiculos/:codigo', service.authenticate, controller.getByCodigo);
router.post('/veiculos', service.authenticate, controller.post);
router.put('/veiculos/:codigo', service.authenticate, controller.put);
router.delete('/veiculos/:codigo', service.authenticate, controller.delete);

module.exports = router;