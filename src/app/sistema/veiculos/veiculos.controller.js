
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const VeiculosModel = require('./veiculos.model');

module.exports = {
  post: (req, res) => {
    const veiculosModel = new VeiculosModel(req.body);
    veiculosModel.validateToInsert();

    if(!veiculosModel.isValid())
      return res.send(new Response(false, veiculosModel.errors, null));

    veiculosModel.transformToInsert();
    veiculosModel.veiculo.loja_id = res.token.loja_id;

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      if(veiculosModel.veiculo.cliente_codigo) {
        connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [veiculosModel.veiculo.cliente_codigo, res.token.loja_id], (error, results) => {
          if(error) { connection.release(); return res.send(new Response(false, error, null)); }
          veiculosModel.veiculo.cliente_id = results[0].id;
          delete veiculosModel.veiculo.cliente_codigo;
          connection.query('INSERT INTO veiculo SET ?', [veiculosModel.veiculo], (error, results) => {
            if(error) { connection.release(); return res.send(new Response(false, error, null)); }
            connection.query('SELECT codigo FROM veiculo WHERE id = ?', [results.insertId], (error, results) => {
              connection.release();
              if(error) return res.send(new Response(false, error, null));
              res.send(new Response(true, 'O veículo foi adicionado com sucesso.', results[0]));
            });
          });
        });
      }
      else {
        veiculosModel.veiculo.cliente_id = null;
        delete veiculosModel.veiculo.cliente_codigo;
        connection.query('INSERT INTO veiculo SET ?', [veiculosModel.veiculo], (error, results) => {
          if(error) { connection.release(); return res.send(new Response(false, error, null)); }
          connection.query('SELECT codigo FROM veiculo WHERE id = ?', [results.insertId], (error, results) => {
            connection.release();
            if(error) return res.send(new Response(false, error, null));
            res.send(new Response(true, 'O veículo foi adicionado com sucesso.', results[0]));
          });
        });
      }
    });
  },

  put: (req, res) => {
    const veiculosModel = new VeiculosModel(req.body);
    veiculosModel.validateToUpdate();

    if(!veiculosModel.isValid())
      return res.send(new Response(false, veiculosModel.errors, null));

    veiculosModel.transformToUpdate();

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      if(veiculosModel.veiculo.cliente_codigo) {
        connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [veiculosModel.veiculo.cliente_codigo, res.token.loja_id], (error, results) => {
          if(error) { connection.release(); return res.send(new Response(false, error, null)); }
          veiculosModel.veiculo.cliente_id = results[0].id;
          delete veiculosModel.veiculo.cliente_codigo;
          connection.query('UPDATE veiculo SET ? WHERE codigo = ? AND loja_id = ?', [veiculosModel.veiculo, req.params.codigo, res.token.loja_id], (error, results) => {
            connection.release();
            if(error) return res.send(new Response(false, error, null));
            res.send(new Response(true, 'O veículo foi alterado com sucesso.', results));
          });
        });
      }
      else {
        veiculosModel.veiculo.cliente_id = null;
        delete veiculosModel.veiculo.cliente_codigo;
        connection.query('UPDATE veiculo SET ? WHERE codigo = ? AND loja_id = ?', [veiculosModel.veiculo, req.params.codigo, res.token.loja_id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O veículo foi alterado com sucesso.', results));
        });
      }
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('DELETE FROM veiculo WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O veículo foi excluído com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    let filter, search, entries, query;
    filter = req.query.filter || '';
    search = req.query.search || '';
    if(filter && (filter === 'codigo')) {
      entries = [filter, search, res.token.loja_id];
      query = `
        SELECT
          veiculo.codigo, veiculo.novo_usado, veiculo.consignado, veiculo.ano_modelo, veiculo.versao, veiculo.exercicio, veiculo.exercicio_data_emissao, veiculo.placa, veiculo.renavam, veiculo.ano_fabricacao, veiculo.chassi,
          marca.id AS marca_id, marca.nome AS marca_nome,
          modelo.id AS modelo_id, modelo.nome AS modelo_nome,
          cor.id AS cor_id, cor.nome AS cor_nome,
          combustivel.id AS combustivel_id, combustivel.nome AS combustivel_nome,
          cliente.codigo AS cliente_codigo, cliente.pessoa, cliente.cpf_cnpj, cliente.nome
        FROM veiculo
            INNER JOIN marca ON veiculo.marca_id = marca.id
            INNER JOIN modelo ON veiculo.modelo_id = modelo.id
            LEFT JOIN cor ON veiculo.cor_id = cor.id
            LEFT JOIN combustivel ON veiculo.combustivel_id = combustivel.id
            LEFT JOIN cliente ON veiculo.cliente_id = cliente.id
        WHERE
          ?? LIKE ? AND veiculo.loja_id = ?`;
    }
    else {
      entries = [res.token.loja_id];
      query = `
        SELECT
          veiculo.codigo, veiculo.novo_usado, veiculo.consignado, veiculo.ano_modelo, veiculo.versao, veiculo.exercicio, veiculo.exercicio_data_emissao, veiculo.placa, veiculo.renavam, veiculo.ano_fabricacao, veiculo.chassi,
          marca.id AS marca_id, marca.nome AS marca_nome,
          modelo.id AS modelo_id, modelo.nome AS modelo_nome,
          cor.id AS cor_id, cor.nome AS cor_nome,
          combustivel.id AS combustivel_id, combustivel.nome AS combustivel_nome,
          cliente.codigo AS cliente_codigo, cliente.pessoa, cliente.cpf_cnpj, cliente.nome
        FROM veiculo
            INNER JOIN marca ON veiculo.marca_id = marca.id
            INNER JOIN modelo ON veiculo.modelo_id = modelo.id
            LEFT JOIN cor ON veiculo.cor_id = cor.id
            LEFT JOIN combustivel ON veiculo.combustivel_id = combustivel.id
            LEFT JOIN cliente ON veiculo.cliente_id = cliente.id
        WHERE
          veiculo.loja_id = ?`;
    }
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query(query, entries, (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os veículos foram retornados com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      const query = `
        SELECT
          veiculo.codigo, veiculo.novo_usado, veiculo.consignado, veiculo.ano_modelo, veiculo.versao, veiculo.exercicio, veiculo.exercicio_data_emissao, veiculo.placa, veiculo.renavam, veiculo.ano_fabricacao, veiculo.chassi,
          marca.id AS marca_id, marca.nome AS marca_nome,
          modelo.id AS modelo_id, modelo.nome AS modelo_nome,
          cor.id AS cor_id, cor.nome AS cor_nome,
          combustivel.id AS combustivel_id, combustivel.nome AS combustivel_nome,
          cliente.codigo AS cliente_codigo, cliente.pessoa, cliente.cpf_cnpj, cliente.nome
        FROM veiculo
            INNER JOIN marca ON veiculo.marca_id = marca.id
            INNER JOIN modelo ON veiculo.modelo_id = modelo.id
            LEFT JOIN cor ON veiculo.cor_id = cor.id
            LEFT JOIN combustivel ON veiculo.combustivel_id = combustivel.id
            LEFT JOIN cliente ON veiculo.cliente_id = cliente.id
        WHERE
          veiculo.codigo = ? AND veiculo.loja_id = ?`;
      connection.query(query, [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O veículo foi retornado com sucesso.', results[0]));
      });
    });
  }

};