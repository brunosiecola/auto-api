
const DataValidator = require('./../../../validators/data.validator');

class VeiculosModel extends DataValidator {

  constructor(veiculo) {
    super();
    this.veiculo = {
      novo_usado: veiculo.novo_usado || null,
      consignado: veiculo.consignado || null,
      marca_id: veiculo.marca_id || null,
      modelo_id: veiculo.modelo_id || null,
      ano_modelo: veiculo.ano_modelo || null,
      versao: veiculo.versao || null,
      combustivel_id: veiculo.combustivel_id || null,
      cor_id: veiculo.cor_id || null,
      cliente_codigo: veiculo.cliente_codigo || null,
      exercicio: veiculo.exercicio || null,
      exercicio_data_emissao: veiculo.exercicio_data_emissao || null,
      placa: veiculo.placa || null,
      renavam: veiculo.renavam || null,
      ano_fabricacao: veiculo.ano_fabricacao || null,
      chassi: veiculo.chassi || null
    }
  }

  validateNovo() {
    this.isRequired(this.veiculo.novo_usado, 'O campo novo_usado deve ser informado.');
    if(this.veiculo.novo_usado && (this.veiculo.novo_usado != 'N' && this.veiculo.novo_usado != 'U')) {
      this.errors.push('O campo novo_usado deve ter como valor N ou U.');
    }
  }

  validateConsignado() {
    this.isRequired(this.veiculo.consignado, 'O campo consignado deve ser informado.');
    if(this.veiculo.consignado && (this.veiculo.consignado != '0' && this.veiculo.consignado != '1')) {
      this.errors.push('O campo consignado deve ter como valor 0 ou 1.');
    }
  }

  validateMarcaId() {
    this.isRequired(this.veiculo.marca_id, 'O campo marca_id deve ser informado.');
  }

  validateModeloId() {
    this.isRequired(this.veiculo.modelo_id, 'O campo modelo_id deve ser informado.');
  }

  validateToInsert() {
    this.validateNovo();
    this.validateConsignado();
    this.validateMarcaId();
    this.validateModeloId();
  }

  validateToUpdate() {
    this.validateNovo();
    this.validateConsignado();
    this.validateMarcaId();
    this.validateModeloId();
  }

  transformToInsert() {
    this.veiculo.versao = this.setUpperCase(this.veiculo.versao);
    this.veiculo.placa = this.setUpperCase(this.veiculo.placa);
    this.veiculo.chassi = this.setUpperCase(this.veiculo.chassi);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = VeiculosModel;
