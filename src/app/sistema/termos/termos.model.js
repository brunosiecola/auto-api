
const DataValidator = require('./../../../validators/data.validator');

class TermosModel extends DataValidator {

  constructor(termo) {
    super();
    this.termo = {
      nome: termo.nome || null,
      texto: termo.texto || null
    }
  }

  validateNome() {
    this.isRequired(this.termo.nome, 'O campo nome deve ser informado.');
    if(this.termo.nome) {
      this.hasMaxLength(this.termo.nome, 50, 'O campo nome pode ter até 50 caracteres.');
    }
  }

  validateTexto() {
    this.isRequired(this.termo.texto, 'O campo texto deve ser informado.');
    if(this.termo.texto) {
      this.hasMaxLength(this.termo.texto, 5000, 'O campo texto pode ter até 5000 caracteres.');
    }
  }

  validateToInsert() {
    this.validateNome();
    this.validateTexto();
  }

  validateToUpdate() {
    this.validateToInsert();
  }

  transformToInsert() {
    this.termo.nome = this.setUpperCase(this.termo.nome);
    this.termo.texto = this.setUpperCase(this.termo.texto);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = TermosModel;
