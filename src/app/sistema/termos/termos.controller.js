
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const TermosModel = require('./termos.model');

module.exports = {
  post: (req, res) => {
    const termosModel = new TermosModel(req.body);
    termosModel.validateToInsert();

    if(!termosModel.isValid())
      return res.send(new Response(false, termosModel.errors, null));

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      termosModel.transformToInsert();
      termosModel.termo.loja_id = res.token.loja_id;
      connection.query('INSERT INTO termo SET ?', [termosModel.termo], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('SELECT codigo FROM termo WHERE id = ?', [results.insertId], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O termo foi adicionado com sucesso.', results[0]));
        });
      });
    });
  },

  put: (req, res) => {
    const termosModel = new TermosModel(req.body);
    termosModel.validateToUpdate();

    if(!termosModel.isValid())
      return res.send(new Response(false, termosModel.errors, null));

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      termosModel.transformToUpdate();
      connection.query('UPDATE termo SET ? WHERE codigo = ? AND loja_id = ?', [termosModel.termo, req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O termo foi alterado com sucesso.', results));
      });
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('DELETE FROM termo WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O termo foi excluído com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    let filter, search, entries, query;
    filter = req.query.filter || '';
    search = req.query.search || '';
    if(filter && (filter === 'codigo' || filter === 'nome')) {
      if(filter === 'nome') search = '%' + search + '%';
      entries = [res.token.loja_id, filter, search];
      query = 'SELECT codigo, nome, texto FROM termo WHERE loja_id = ? AND ?? LIKE ?';
    }
    else {
      entries = [res.token.loja_id];
      query = 'SELECT codigo, nome, texto FROM termo WHERE loja_id = ?';
    }
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query(query, entries, (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os termos foram retornados com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT codigo, nome, texto FROM termo WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O termo foi retornado com sucesso.', results[0]));
      });
    });
  }

};