
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./termos.controller');

router.get('/termos', service.authenticate, controller.get);
router.get('/termos/:codigo', service.authenticate, controller.getByCodigo);
router.post('/termos', service.authenticate, controller.post);
router.put('/termos/:codigo', service.authenticate, controller.put);
router.delete('/termos/:codigo', service.authenticate, controller.delete);

module.exports = router;