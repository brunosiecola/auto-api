
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./funcionarios.controller');

router.get('/funcionarios', service.authenticate, controller.get);
router.get('/funcionarios/:codigo', service.authenticate, controller.getByCodigo);
router.post('/funcionarios', service.authenticate, controller.post);
router.put('/funcionarios/:codigo', service.authenticate, controller.put);
router.delete('/funcionarios/:codigo', service.authenticate, controller.delete);

module.exports = router;