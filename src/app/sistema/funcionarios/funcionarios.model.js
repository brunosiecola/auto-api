
const DataValidator = require('./../../../validators/data.validator');

class FuncionariosModel extends DataValidator {

  constructor(funcionario) {
    super();
    this.funcionario = {
      nome: funcionario.nome || null,
      comprador: funcionario.comprador || null,
      vendedor: funcionario.vendedor || null,
      status: funcionario.status || null
    }
  }

  validateNome() {
    this.isRequired(this.funcionario.nome, 'O campo nome deve ser informado.');
    if(this.funcionario.nome) {
      this.hasMaxLength(this.funcionario.nome, 100, 'O campo nome pode ter até 100 caracteres.');
    }
  }

  validateComprador() {
    this.isRequired(this.funcionario.comprador, 'O campo comprador deve ser informado.');
    if(this.funcionario.comprador && (this.funcionario.comprador != '0' && this.funcionario.comprador != '1')) {
      this.errors.push('O campo comprador deve ter como valor 0 ou 1.');
    }
  }

  validateVendedor() {
    this.isRequired(this.funcionario.vendedor, 'O campo vendedor deve ser informado.');
    if(this.funcionario.vendedor && (this.funcionario.vendedor != '0' && this.funcionario.vendedor != '1')) {
      this.errors.push('O campo vendedor deve ter como valor 0 ou 1.');
    }
  }

  validateStatus() {
    this.isRequired(this.funcionario.status, 'O campo status deve ser informado.');
    if(this.funcionario.status && (this.funcionario.status != '0' && this.funcionario.status != '1')) {
      this.errors.push('O campo status deve ter como valor 0 ou 1.');
    }
  }

  validateToInsert() {
    this.validateNome();
    this.validateComprador();
    this.validateVendedor();
    this.validateStatus();
  }

  validateToUpdate() {
    this.validateNome();
    this.validateComprador();
    this.validateVendedor();
    this.validateStatus();
  }

  transformToInsert() {
    this.funcionario.nome = this.setUpperCase(this.funcionario.nome);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = FuncionariosModel;
