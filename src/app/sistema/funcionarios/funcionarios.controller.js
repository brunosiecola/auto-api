
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const FuncionariosModel = require('./funcionarios.model');

module.exports = {
  post: (req, res) => {
    const funcionariosModel = new FuncionariosModel(req.body);
    funcionariosModel.validateToInsert();

    if(!funcionariosModel.isValid())
      return res.send(new Response(false, funcionariosModel.errors, null));

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      funcionariosModel.transformToInsert();
      funcionariosModel.funcionario.loja_id = res.token.loja_id;
      connection.query('INSERT INTO funcionario SET ?', [funcionariosModel.funcionario], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('SELECT codigo FROM funcionario WHERE id = ?', [results.insertId], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O funcionário foi adicionado com sucesso.', results[0]));
        });
      });
    });
  },

  put: (req, res) => {
    const funcionariosModel = new FuncionariosModel(req.body);
    funcionariosModel.validateToUpdate();

    if(!funcionariosModel.isValid())
      return res.send(new Response(false, funcionariosModel.errors, null));

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      funcionariosModel.transformToUpdate();
      connection.query('UPDATE funcionario SET ? WHERE codigo = ? AND loja_id = ?', [funcionariosModel.funcionario, req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O funcionário foi alterado com sucesso.', results));
      });
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('DELETE FROM funcionario WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O funcionário foi excluído com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    let filter, search, entries, query;
    filter = req.query.filter || '';
    search = req.query.search || '';
    if(filter && (filter === 'codigo' || filter === 'nome' || filter === 'comprador' || filter === 'vendedor' || filter === 'status')) {
      if(filter === 'nome') search = '%' + search + '%';
      entries = [res.token.loja_id, filter, search];
      query = 'SELECT codigo, nome, comprador, vendedor, status FROM funcionario WHERE loja_id = ? AND ?? LIKE ?';
    }
    else {
      entries = [res.token.loja_id];
      query = 'SELECT codigo, nome, comprador, vendedor, status FROM funcionario WHERE loja_id = ?';
    }
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query(query, entries, (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os funcionários foram retornados com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT codigo, nome, comprador, vendedor, status FROM funcionario WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O funcionário foi retornado com sucesso.', results[0]));
      });
    });
  }

};