
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const LojaModel = require('./loja.model');

module.exports = {

  put: (req, res) => {
    const lojaModel = new LojaModel(req.body);
    lojaModel.validateToUpdate();

    if(!lojaModel.isValid())
      return res.send(new Response(false, lojaModel.errors, null));
      
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('UPDATE loja SET ? WHERE id = ?', [lojaModel.loja, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os dados da sua loja foram alterados com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id, cnpj, ie, nome, cep, estado, cidade, bairro, logradouro, numero, complemento, status FROM loja WHERE id = ?', [res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os dados da sua loja foram retornados com sucesso.', results[0]));
      });
    });
  }

};
