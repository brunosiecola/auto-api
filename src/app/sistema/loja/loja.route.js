
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./loja.controller');

router.get('/loja', service.authenticate, controller.get);
router.put('/loja', service.authenticate, controller.put);

module.exports = router;
