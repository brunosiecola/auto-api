
const DataValidator = require('./../../../validators/data.validator');

class LojaModel extends DataValidator {

  constructor(loja) {
    super();
    this.loja = {
      cnpj: loja.cnpj || null,
      ie: loja.ie || null,
      nome: loja.nome || null,
      cep: loja.cep || null,
      estado: loja.estado || null,
      cidade: loja.cidade || null,
      bairro: loja.bairro || null,
      logradouro: loja.logradouro || null,
      numero: loja.numero || null,
      complemento: loja.complemento || null
    }
  }

  validateCnpj() {
    this.isRequired(this.loja.cnpj, 'O campo cnpj deve ser informado.');
    if(this.loja.cnpj) {
      this.isFixedLength(this.loja.cnpj, 15, 'O campo cnpj deve ter 15 caracteres.');
    }
  }

  validateIe() {
    if(this.loja.ie) {
      this.isFixedLength(this.loja.ie, 12, 'O campo ie deve ter 12 caracteres.');
    }
  }

  validateNome() {
    this.isRequired(this.loja.nome, 'O campo nome deve ser informado.');
    if(this.loja.nome) {
      this.hasMaxLength(this.loja.nome, 100, 'O campo nome pode ter até 100 caracteres.');
    }
  }

  validateCep() {
    if(this.loja.cep) {
      this.isFixedLength(this.loja.cep, 8, 'O campo cep deve ter 8 caracteres.');
    }
  }

  validateEstado() {
    if(this.loja.estado) {
      this.isFixedLength(this.loja.estado, 2, 'O campo estado deve ter 2 caracteres.');
    }
  }

  validateCidade() {
    if(this.loja.cidade) {
      this.hasMaxLength(this.loja.cidade, 100, 'O campo cidade pode ter até 100 caracteres.');
    }
  }

  validateBairro() {
    if(this.loja.bairro) {
      this.hasMaxLength(this.loja.bairro, 100, 'O campo bairro pode ter até 100 caracteres.');
    }
  }

  validateLogradouro() {
    if(this.loja.logradouro) {
      this.hasMaxLength(this.loja.logradouro, 100, 'O campo logradouro pode ter até 100 caracteres.');
    }
  }

  validateNumero() {
    if(this.loja.numero) {
      this.hasMaxLength(this.loja.numero, 100, 'O campo numero pode ter até 100 caracteres.');
    }
  }

  validateComplemento() {
    if(this.loja.complemento) {
      this.hasMaxLength(this.loja.complemento, 100, 'O campo complemento pode ter até 100 caracteres.');
    }
  }

  validateToUpdate() {
    this.validateCnpj();
    this.validateIe();
    this.validateNome();
    this.validateCep();
    this.validateEstado();
    this.validateCidade();
    this.validateBairro();
    this.validateLogradouro();
    this.validateNumero();
    this.validateComplemento();
  }

}

module.exports = LojaModel;
