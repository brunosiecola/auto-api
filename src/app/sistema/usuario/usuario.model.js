
const DataValidator = require('./../../../validators/data.validator');

class UsuarioModel extends DataValidator {

  constructor(usuario) {
    super();
    this.usuario = {
      nome: usuario.nome || null,
      usuario: usuario.usuario || null,
      senha: usuario.senha || null
    }
  }

  validateNome() {
    this.isRequired(this.usuario.nome, 'O campo nome deve ser informado.');
    if(this.usuario.nome) {
      this.hasMaxLength(this.usuario.nome, 100, 'O campo nome pode ter até 100 caracteres.');
    }
  }

  validateUsuario() {
    this.isRequired(this.usuario.usuario, 'O campo usuário deve ser informado.');
    if(this.usuario.usuario) {
      this.hasMaxLength(this.usuario.usuario, 20, 'O campo usuário pode ter até 20 caracteres.');
    }
  }

  validateSenha() {
    if(this.usuario.senha) {
      this.hasMaxLength(this.usuario.senha, 20, 'O campo senha pode ter até 20 caracteres.');
    }
  }

  validateToUpdate() {
    this.validateNome();
    this.validateUsuario();
    this.validateSenha();
  }

}

module.exports = UsuarioModel;
