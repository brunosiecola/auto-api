
const bcrypt = require('bcrypt');
const config = require('./../../../config');
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const UsuarioModel = require('./usuario.model');

module.exports = {

  put: (req, res) => {
    const usuarioModel = new UsuarioModel(req.body);
    usuarioModel.validateToUpdate();

    if(!usuarioModel.isValid())
      return res.send(new Response(false, usuarioModel.errors, null));

    if(usuarioModel.usuario.senha)
      usuarioModel.usuario.senha = bcrypt.hashSync(usuarioModel.usuario.senha, config.bcrypt.salt);
    else
      delete usuarioModel.usuario.senha;

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('UPDATE usuario SET ? WHERE id = ?', [usuarioModel.usuario, res.token.usuario_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os dados do seu usuário foram alterados com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id, codigo, nome, usuario, status FROM usuario WHERE id = ?', [res.token.usuario_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os dados do seu usuário foram retornados com sucesso.', results[0]));
      });
    });
  }

};
