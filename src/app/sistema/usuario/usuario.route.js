
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./usuario.controller');

router.get('/usuario', service.authenticate, controller.get);
router.put('/usuario', service.authenticate, controller.put);

module.exports = router;
