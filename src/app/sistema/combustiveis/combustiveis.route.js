
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./combustiveis.controller');

router.get('/combustiveis', service.authenticate, controller.get);
router.get('/combustiveis/:combustivel_id', service.authenticate, controller.getByCodigo);

module.exports = router;