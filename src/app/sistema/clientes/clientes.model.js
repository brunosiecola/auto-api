
const DataValidator = require('./../../../validators/data.validator');

class ClientesModel extends DataValidator {

  constructor(cliente) {
    super();
    this.cliente = {
      pessoa: cliente.pessoa || null,
      cpf_cnpj: cliente.cpf || cliente.cnpj || null,
      rg_ie: cliente.rg || cliente.ie || null,
      nome: cliente.nome || null,
      data_nascimento: cliente.data_nascimento || null,
      cep: cliente.cep || null,
      estado: cliente.estado || null,
      cidade: cliente.cidade || null,
      bairro: cliente.bairro || null,
      logradouro: cliente.logradouro || null,
      numero: cliente.numero || null,
      complemento: cliente.complemento || null,
      observacoes: cliente.observacoes || null
    }
  }

  validatePessoa() {
    this.isRequired(this.cliente.pessoa, 'O campo pessoa deve ser informado.');
    if(this.cliente.pessoa && (this.cliente.pessoa !== 'F' && this.cliente.pessoa !== 'J')) {
      this.errors.push('O campo pessoa deve ter como valor F ou J.');
    }
  }

  validateCpfCnpj() {
    this.isRequired(this.cliente.cpf_cnpj, 'O campo cpf_cnpj deve ser informado.');
    if(this.cliente.pessoa === 'F') {
      if(this.cliente.cpf_cnpj) {
        this.isFixedLength(this.cliente.cpf_cnpj, 11, 'O campo cpf_cnpj deve ter 11 caracteres.');
      }
    }
    else if(this.cliente.pessoa === 'J') {
      if(this.cliente.cpf_cnpj) {
        this.isFixedLength(this.cliente.cpf_cnpj, 15, 'O campo cpf_cnpj deve ter 15 caracteres.');
      }
    }
  }

  validateRgIe() {
    if(this.cliente.rg_ie) {
      if(this.cliente.pessoa === 'F') {
        if(this.cliente.rg_ie) {
          this.hasMaxLength(this.cliente.rg_ie, 12, 'O campo rg_ie pode ter até 12 caracteres.');
        }
      }
      else if(this.cliente.pessoa === 'J') {
        if(this.cliente.rg_ie) {
          this.isFixedLength(this.cliente.rg_ie, 12, 'O campo rg_ie deve ter 12 caracteres.');
        }
      }
    }
  }

  validateNome() {
    this.isRequired(this.cliente.nome, 'O campo nome deve ser informado.');
    if(this.cliente.nome) {
      this.hasMaxLength(this.cliente.nome, 100, 'O campo nome pode ter até 100 caracteres.');
    }
  }

  validateDataNascimento() {
    if(this.cliente.data_nascimento) {
      this.isFixedLength(this.cliente.data_nascimento, 10, 'O campo data_nascimento deve ter 10 caracteres.');
      this.isDate(this.cliente.data_nascimento, 'O campo data_nascimento deve ter o formato correto.');
    }
  }

  validateCep() {
    if(this.cliente.cep) {
      this.isFixedLength(this.cliente.cep, 8, 'O campo cep deve ter 8 caracteres.');
    }
  }

  validateEstado() {
    if(this.cliente.estado) {
      this.isFixedLength(this.cliente.estado, 2, 'O campo estado deve ter 2 caracteres.');
    }
  }

  validateCidade() {
    if(this.cliente.cidade) {
      this.hasMaxLength(this.cliente.cidade, 100, 'O campo cidade pode ter até 100 caracteres.');
    }
  }

  validateBairro() {
    if(this.cliente.bairro) {
      this.hasMaxLength(this.cliente.bairro, 100, 'O campo bairro pode ter até 100 caracteres.');
    }
  }

  validateLogradouro() {
    if(this.cliente.logradouro) {
      this.hasMaxLength(this.cliente.logradouro, 100, 'O campo logradouro pode ter até 100 caracteres.');
    }
  }

  validateNumero() {
    if(this.cliente.numero) {
      this.hasMaxLength(this.cliente.numero, 100, 'O campo numero pode ter até 100 caracteres.');
    }
  }

  validateComplemento() {
    if(this.cliente.complemento) {
      this.hasMaxLength(this.cliente.complemento, 100, 'O campo complemento pode ter até 100 caracteres.');
    }
  }

  validateObservacoes() {
    if(this.cliente.observacoes) {
      this.hasMaxLength(this.cliente.observacoes, 500, 'O campo observacoes pode ter até 500 caracteres.');
    }
  }

  validateToInsert() {
    this.validatePessoa();
    this.validateCpfCnpj();
    this.validateRgIe();
    this.validateNome();
    this.validateDataNascimento();
    this.validateCep();
    this.validateEstado();
    this.validateCidade();
    this.validateBairro();
    this.validateLogradouro();
    this.validateNumero();
    this.validateComplemento();
    this.validateObservacoes();
  }

  validateToUpdate() {
    this.validateToInsert();
  }

  transformToInsert() {
    this.cliente.pessoa = this.setUpperCase(this.cliente.pessoa);
    this.cliente.rg_ie = this.setUpperCase(this.cliente.rg_ie);
    this.cliente.nome = this.setUpperCase(this.cliente.nome);
    this.cliente.estado = this.setUpperCase(this.cliente.estado);
    this.cliente.cidade = this.setUpperCase(this.cliente.cidade);
    this.cliente.bairro = this.setUpperCase(this.cliente.bairro);
    this.cliente.logradouro = this.setUpperCase(this.cliente.logradouro);
    this.cliente.numero = this.setUpperCase(this.cliente.numero);
    this.cliente.complemento = this.setUpperCase(this.cliente.complemento);
    this.cliente.observacoes = this.setUpperCase(this.cliente.observacoes);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = ClientesModel;