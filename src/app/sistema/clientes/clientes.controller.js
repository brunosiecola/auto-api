
const pool = require('./../../../database');
const Response = require('./../../../models/response.model');
const ClientesModel = require('./clientes.model');

module.exports = {

  post: (req, res) => {

    const clientesModel = new ClientesModel(req.body);
    clientesModel.validateToInsert();

    if(!clientesModel.isValid())
      return res.send(new Response(false, clientesModel.errors, null));
    
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT pessoa FROM cliente WHERE cpf_cnpj = ? AND loja_id = ?', [clientesModel.cliente.cpf_cnpj, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        let cpfCnpj = (results[0] && results[0].pessoa === 'F') ? 'CPF' : 'CNPJ';
        if(results[0]) return res.send(new Response(false, 'Já existe um cliente cadastrado com o ' + cpfCnpj + ' informado.', null));
        clientesModel.transformToInsert();
        clientesModel.cliente.loja_id = res.token.loja_id;
        connection.query('INSERT INTO cliente SET ?', [clientesModel.cliente], (error, results) => {
          if(error) {
            connection.release();
            return res.send(new Response(false, error, null));
          }
          connection.query('SELECT codigo FROM cliente WHERE id = ?', [results.insertId], (error, results) => {
            connection.release();
            if(error) return res.send(new Response(false, error, null));
            res.send(new Response(true, 'O cliente foi adicionado com sucesso.', { codigo: results[0].codigo }));
          });
        });
      });
    });

  },

  put: (req, res) => {
    const clientesModel = new ClientesModel(req.body);
    clientesModel.validateToUpdate();

    if(!clientesModel.isValid())
      return res.send(new Response(false, clientesModel.errors, null));

    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT pessoa FROM cliente WHERE codigo != ? AND cpf_cnpj = ? AND loja_id = ?', [req.params.codigo, clientesModel.cliente.cpf_cnpj, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        let cpfCnpj = (results[0] && results[0].pessoa === 'F') ? 'CPF' : 'CNPJ';
        if(results[0]) return res.send(new Response(false, 'Já existe um cliente cadastrado com o ' + cpfCnpj + ' informado.', null));
        clientesModel.transformToUpdate();
        connection.query('UPDATE cliente SET ? WHERE codigo = ? AND loja_id = ?', [clientesModel.cliente, req.params.codigo, res.token.loja_id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O cliente foi alterado com sucesso.', results));
        });
      });
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('DELETE FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O cliente foi excluído com sucesso.', results));
      });
    });
  },

  get: (req, res) => {
    let filter, search, entries, query;
    filter = req.query.filter || '';
    search = req.query.search || '';
    if(filter && (filter === 'codigo' || filter === 'nome' || filter === 'cpf_cnpj')) {
      if(filter === 'nome') search = '%' + search + '%';
      entries = [res.token.loja_id, filter, search];
      query = 'SELECT * FROM cliente WHERE loja_id = ? AND ?? LIKE ?';
    }
    else {
      entries = [res.token.loja_id];
      query = 'SELECT * FROM cliente WHERE loja_id = ?';
    }
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query(query, entries, (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'Os clientes foram retornados com sucesso.', results));
      });
    });
  },

  getByCodigo: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT codigo, nome, pessoa, cpf_cnpj, rg_ie, nome, data_nascimento, cep, estado, cidade, bairro, logradouro, numero, complemento, observacoes FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        connection.release();
        if(error) return res.send(new Response(false, error, null));
        res.send(new Response(true, 'O cliente foi retornado com sucesso.', results[0]));
      });
    });
  }

};
