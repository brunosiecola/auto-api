
const express = require('express');
const router = express.Router({ mergeParams: true });
const service = require('./../../authentication.service');
const controller = require('./emails.controller');

router.get('/emails', service.authenticate, controller.get);
router.post('/emails', service.authenticate, controller.post);
router.put('/emails/:id', service.authenticate, controller.put);
router.delete('/emails/:id', service.authenticate, controller.delete);

module.exports = router;
