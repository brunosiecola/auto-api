
const DataValidator = require('./../../../../validators/data.validator');

class EmailsModel extends DataValidator {

  constructor(email) {
    super();
    this.email = {
      email: email.email,
      descricao: email.descricao
    }
  }

  validateEmail() {
    this.isRequired(this.email.email, 'O campo email deve ser informado.');
    if(this.email.email) {
      this.isEmail(this.email.email, 'O campo email deve ter o formato correto.');
      this.hasMaxLength(this.email.email, 100, 'O campo email pode ter até 100 caracteres.');
    }
  }

  validateDescricao() {
    if(this.email.descricao) {
      this.hasMaxLength(this.email.descricao, 100, 'O campo descricao pode ter até 100 caracteres.');
    }
  }

  validateToInsert() {
    this.validateEmail();
    this.validateDescricao();
  }

  validateToUpdate() {
    this.validateEmail();
    this.validateDescricao();
  }

  transformToInsert() {
    this.email.email = this.setUpperCase(this.email.email);
    this.email.descricao = this.setUpperCase(this.email.descricao);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = EmailsModel;
