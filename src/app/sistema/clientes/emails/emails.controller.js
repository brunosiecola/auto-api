
const pool = require('./../../../../database');
const Response = require('./../../../../models/response.model');
const EmailsModel = require('./emails.model');

module.exports = {

  post: (req, res) => {
    const emailsModel = new EmailsModel(req.body);
    emailsModel.validateToInsert();

    if(!emailsModel.isValid())
      return res.send(new Response(false, emailsModel.errors, null));
    
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        emailsModel.transformToInsert();
        emailsModel.email.cliente_id = results[0].id;
        connection.query('INSERT INTO cliente_email SET ?', [emailsModel.email], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O e-mail do cliente foi adicionado com sucesso.', results));
        });
      });
    });
  },

  put: (req, res) => {
    const emailsModel = new EmailsModel(req.body);
    emailsModel.validateToInsert();

    if(!emailsModel.isValid())
      return res.send(new Response(false, emailsModel.errors, null));
    
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        emailsModel.transformToUpdate();
        connection.query('UPDATE cliente_email SET ? WHERE id = ? AND cliente_id = ?', [emailsModel.email, req.params.id, results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O e-mail do cliente foi alterado com sucesso.', results));
        });
      });
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('DELETE FROM cliente_email WHERE id = ? AND cliente_id = ?', [req.params.id, results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O e-mail do cliente foi excluído com sucesso.', results));
        });
      });
    });
  },

  get: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('SELECT id, email, descricao FROM cliente_email WHERE cliente_id = ?', [results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'Os e-mails do cliente foram retornados com sucesso.', results));
        });
      });
    });
  }

};