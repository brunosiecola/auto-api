
const express = require('express');
const router = express.Router({ mergeParams: true });
const service = require('./../../authentication.service');
const controller = require('./telefones.controller');

router.get('/telefones', service.authenticate, controller.get);
router.post('/telefones', service.authenticate, controller.post);
router.put('/telefones/:id', service.authenticate, controller.put);
router.delete('/telefones/:id', service.authenticate, controller.delete);

module.exports = router;
