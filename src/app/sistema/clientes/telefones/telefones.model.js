
const DataValidator = require('./../../../../validators/data.validator');

class TelefonesModel extends DataValidator {

  constructor(telefone) {
    super();
    this.telefone = {
      telefone: telefone.telefone,
      descricao: telefone.descricao
    }
  }

  validateEmail() {
    this.isRequired(this.telefone.telefone, 'O campo telefone deve ser informado.');
    if(this.telefone.telefone) {
      this.hasMaxLength(this.telefone.telefone, 11, 'O campo telefone pode ter até 11 caracteres.');
    }
  }

  validateDescricao() {
    if(this.telefone.descricao) {
      this.hasMaxLength(this.telefone.descricao, 100, 'O campo descricao pode ter até 100 caracteres.');
    }
  }

  validateToInsert() {
    this.validateEmail();
    this.validateDescricao();
  }

  validateToUpdate() {
    this.validateEmail();
    this.validateDescricao();
  }

  transformToInsert() {
    this.telefone.descricao = this.setUpperCase(this.telefone.descricao);
  }

  transformToUpdate() {
    this.transformToInsert();
  }

}

module.exports = TelefonesModel;
