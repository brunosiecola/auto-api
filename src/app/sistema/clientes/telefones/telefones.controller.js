
const pool = require('./../../../../database');
const Response = require('./../../../../models/response.model');
const TelefonesModel = require('./telefones.model');

module.exports = {

  post: (req, res) => {
    const telefonesModel = new TelefonesModel(req.body);
    telefonesModel.validateToInsert();

    if(!telefonesModel.isValid())
      return res.send(new Response(false, telefonesModel.errors, null));
    
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        telefonesModel.transformToInsert();
        telefonesModel.telefone.cliente_id = results[0].id;
        connection.query('INSERT INTO cliente_telefone SET ?', [telefonesModel.telefone], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O telefone do cliente foi adicionado com sucesso.', results));
        });
      });
    });
  },

  put: (req, res) => {
    const telefonesModel = new TelefonesModel(req.body);
    telefonesModel.validateToInsert();

    if(!telefonesModel.isValid())
      return res.send(new Response(false, telefonesModel.errors, null));
    
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        telefonesModel.transformToUpdate();
        connection.query('UPDATE cliente_telefone SET ? WHERE id = ? AND cliente_id = ?', [telefonesModel.telefone, req.params.id, results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O telefone do cliente foi alterado com sucesso.', results));
        });
      });
    });
  },

  delete: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('DELETE FROM cliente_telefone WHERE id = ? AND cliente_id = ?', [req.params.id, results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'O telefone do cliente foi excluído com sucesso.', results));
        });
      });
    });
  },

  get: (req, res) => {
    pool.getConnection((error, connection) => {
      if(error) return res.send(new Response(false, error, null));
      connection.query('SELECT id FROM cliente WHERE codigo = ? AND loja_id = ?', [req.params.codigo, res.token.loja_id], (error, results) => {
        if(error) {
          connection.release();
          return res.send(new Response(false, error, null));
        }
        connection.query('SELECT id, telefone, descricao FROM cliente_telefone WHERE cliente_id = ?', [results[0].id], (error, results) => {
          connection.release();
          if(error) return res.send(new Response(false, error, null));
          res.send(new Response(true, 'Os telefones do cliente foram retornados com sucesso.', results));
        });
      });
    });
  }

};