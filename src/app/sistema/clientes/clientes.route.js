
const express = require('express');
const router = express.Router();
const service = require('./../authentication.service');
const controller = require('./clientes.controller');
const emailsRouter = require('./emails/emails.route');
const telefonesRouter = require('./telefones/telefones.route');

router.get('/clientes', service.authenticate, controller.get);
router.get('/clientes/:codigo', service.authenticate, controller.getByCodigo);
router.post('/clientes', service.authenticate, controller.post);
router.put('/clientes/:codigo', service.authenticate, controller.put);
router.delete('/clientes/:codigo', service.authenticate, controller.delete);

router.use('/clientes/:codigo', emailsRouter);
router.use('/clientes/:codigo', telefonesRouter);

module.exports = router;
