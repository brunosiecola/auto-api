
const mysql = require('mysql');
const config = require('./config');

const pool = mysql.createPool({
  connectionLimit: 10,
  host: config.mysql.host,
  port: config.mysql.port,
  user: config.mysql.user,
  password: config.mysql.password,
  database: config.mysql.database,
  dateStrings: true
});

module.exports = pool;