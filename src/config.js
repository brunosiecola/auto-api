
module.exports = {

  mysql: {
    host: '127.0.0.1',
    port: 3306,
    database: 'auto',
    user: 'root',
    password: 'root'
  },
  bcrypt: {
    secret: 'E+umjBN)y);YQP,,7=9X',
    salt: 10
  },
  jsonwebtoken: {
    secret: '{?2d}FPvQ/.pMx9u,5c]'
  }

};