
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, token');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  next();
});

// app.use((req, res, next) => {
//   setTimeout(() => {
//     next();
//   }, 1000);
// });

app.get('/api', (req, res) => {
  res.send('Express server is running on port 3000.');
});

const sistemaRouter = require('./app/sistema/sistema.route');

app.use('/api', sistemaRouter);

app.listen(3000, () => {
  console.log('Server is running on port 3000.');
});