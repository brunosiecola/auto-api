
module.exports = class Response {

  constructor(success, message, results) {
    this.success = success;
    this.message = message;
    this.results = results;
  }

};
