
class DataValidator {

  constructor() {
    this.errors = [];
  }

  isRequired(value, message) {
    if(!value || value.length <= 0)
      this.errors.push(message)
  }
  
  hasMaxLength(value, maxLength, message) {
    if(!value || value.length > maxLength)
      this.errors.push(message);
  }

  isFixedLength(value, length, message) {
    if(value.length != length)
      this.errors.push(message);
  }

  isEmail(value, message) {
    const re = new RegExp(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/);
    if(!re.test(value))
      this.errors.push(message);
  }

  isDate(value, message) {
    const re = new RegExp(/[0-9]{4}-[0-9]{2}-[0-9]{2}/);
    if(!re.test(value))
      this.errors.push(message);
  }

  isValid() {
    return this.errors.length === 0;
  }

  setUpperCase(value) {
    if(value) {
      return value.toString().toUpperCase();
    }
    return null;
  }

}

module.exports = DataValidator;
